# Developing

These are notes to test the different components separately while developing.

## Server-side proxy

### 1. Compile and run obfserver

First, we're going to set up the server-side proxy. Do note that we will skip
the part that generates the obfs4 keys (we will use the `obfsproxy/test_data`
folder for testing).

`LHOST` is the local IP of the server (in case it's different than the public IP).
`RHOST` is the OpenVPN gateway IP.

```
export LHOST=10.10.1.10:4430     # this is our obfs4 endpoint (private network; if your public IP is exposed directly you should use the IP:PORT here instead).
export RHOST=163.172.126.44:443  # this is the GW IP (each obfsproxy is routing to only one openvpn GW).

cd server && make build
sudo ./server -addr ${LHOST} -vpn ${RHOST} -state test_data -c test_data/obfs4.json
```
If you want to run in `kcp` mode, at the moment you have to export `KCP=1`. It can be done with:

```
make run-kcp
```

### 2. Run `obfsclient` to start a socks5 proxy in localhost

It will start a local socks5 proxy, to where we will connect with `openvpn`.
For now, this is `TCP` only. By default, the socks5 proxy will listen in `127.0.0.1:8080`.

```
make build-client
make run-client OBFS4_CERT=8nuAbPJwFrKc/29KcCfL5LBuEWxQrjBASYXdUbwcm9d9pKseGK4r2Tg47e23+t6WghxGGw
```

If you want to run in `kcp` mode, at the moment you have to export `KCP=1`. It can be done with:

```
make run-client-kcp OBFS4_CERT=8nuAbPJwFrKc/29KcCfL5LBuEWxQrjBASYXdUbwcm9d9pKseGK4r2Tg47e23+t6WghxGGw
```

### 3. Get certificates for the riseup gateways.

```
make certs
```

You should have certificates in `/tmp/cert.pem`, and the ca file in `/tmp/ca.crt`.


### 4. Run the `openvpn` client using the local `socks5` proxy.

The `openvpn` binary needs to be invoked with the `OBFS4_ENDPOINT_IP` and `OBFS4_ENDPOINT_PORT` as the
`--remote`, the local `socks5` port should be passed with `--proxy`.

In this example, we pass the `OBFS4_ENDPOINT_IP` and `OBFS4_ENDPOINT_PORT` variable via the `Makefile`:

```
make run-openvpn OBFS4_ENDPOINT_IP=2.2.2.2 OBFS4_ENDPOINT_PORT=4430
```

If everything went well, now you should be connected to the gateway remote, and
all routes set up.


```
❯ make run-openvpn OBFS4_ENDPOINT_IP=2.2.2.2 OBFS4_ENDPOINT_PORT=4430
./scripts/run-openvpn-client.sh
+ sudo openvpn --verb 3 --tls-cipher DHE-RSA-AES128-SHA --cipher AES-128-CBC --auth-nocache --proto tcp --dev tun --client --tls-client --remote-cert-tls server --tls-version-min 1.2 --ca /tmp/ca.crt --cert /tmp/cert.pem --key /tmp/cert.pem --pull-filter ignore ifconfig-ipv6 --pull-filter ignore route-ipv6 --socks-proxy 127.0.0.1 8080 --remote 2.2.2.2 443 --route 2.2.2.2 255.255.255.255 net_gateway
2022-05-21 03:41:35 OpenVPN 2.5.1 x86_64-pc-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on Mar 22 2022
2022-05-21 03:41:35 library versions: OpenSSL 1.1.1l  24 Aug 2021, LZO 2.10
2022-05-21 03:41:35 Deprecated TLS cipher name 'DHE-RSA-AES128-SHA', please use IANA name 'TLS-DHE-RSA-WITH-AES-128-CBC-SHA'
2022-05-21 03:41:35 TCP/UDP: Preserving recently used remote address: [AF_INET]127.0.0.1:8080
2022-05-21 03:41:35 Socket Buffers: R=[131072->131072] S=[16384->16384]
2022-05-21 03:41:35 Attempting to establish TCP connection with [AF_INET]127.0.0.1:8080 [nonblock]
2022-05-21 03:41:35 TCP connection established with [AF_INET]127.0.0.1:8080
2022-05-21 03:41:35 TCP_CLIENT link local: (not bound)
2022-05-21 03:41:35 TCP_CLIENT link remote: [AF_INET]127.0.0.1:8080
2022-05-21 03:41:35 TLS: Initial packet from [AF_INET]127.0.0.1:8080, sid=61e85660 36666a7a
2022-05-21 03:41:35 VERIFY OK: depth=1, O=Riseup Networks, OU=https://riseup.net, CN=Riseup Networks Root CA
2022-05-21 03:41:35 VERIFY KU OK
2022-05-21 03:41:35 Validating certificate extended key usage
2022-05-21 03:41:35 ++ Certificate has EKU (str) TLS Web Server Authentication, expects TLS Web Server Authentication
2022-05-21 03:41:35 VERIFY EKU OK
2022-05-21 03:41:35 VERIFY OK: depth=0, CN=mouette.riseup.net, O=Riseup Networks, L=Seattle, ST=WA, C=US
2022-05-21 03:41:36 Control Channel: TLSv1.2, cipher SSLv3 DHE-RSA-AES128-SHA, 4096 bit RSA
2022-05-21 03:41:36 [mouette.riseup.net] Peer Connection Initiated with [AF_INET]127.0.0.1:8080
2022-05-21 03:41:37 SENT CONTROL [mouette.riseup.net]: 'PUSH_REQUEST' (status=1)
2022-05-21 03:41:37 PUSH: Received control message: 'PUSH_REPLY,dhcp-option DNS 10.41.0.1,redirect-gateway def1,route-ipv6 2000::/3,tun-ipv6,route-gateway 10.41.0.1,topology subnet,ping 10,ping-restart 30,socket-flags TCP_NODELAY,ifconfig-ipv6 2001:db8:123::100d/64 2001:db8:123::1,ifconfig 10.41.0.15 255.255.248.0,peer-id 0,cipher AES-256-GCM'
2022-05-21 03:41:37 Pushed option removed by filter: 'route-ipv6 2000::/3'
2022-05-21 03:41:37 Pushed option removed by filter: 'ifconfig-ipv6 2001:db8:123::100d/64 2001:db8:123::1'
2022-05-21 03:41:37 OPTIONS IMPORT: timers and/or timeouts modified
2022-05-21 03:41:37 OPTIONS IMPORT: --socket-flags option modified
2022-05-21 03:41:37 Socket flags: TCP_NODELAY=1 succeeded
2022-05-21 03:41:37 OPTIONS IMPORT: --ifconfig/up options modified
2022-05-21 03:41:37 OPTIONS IMPORT: route options modified
2022-05-21 03:41:37 OPTIONS IMPORT: route-related options modified
2022-05-21 03:41:37 OPTIONS IMPORT: --ip-win32 and/or --dhcp-option options modified
2022-05-21 03:41:37 OPTIONS IMPORT: peer-id set
2022-05-21 03:41:37 OPTIONS IMPORT: adjusting link_mtu to 1626
2022-05-21 03:41:37 OPTIONS IMPORT: data channel crypto options modified
2022-05-21 03:41:37 Data Channel: using negotiated cipher 'AES-256-GCM'
2022-05-21 03:41:37 Outgoing Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
2022-05-21 03:41:37 Incoming Data Channel: Cipher 'AES-256-GCM' initialized with 256 bit key
2022-05-21 03:41:37 net_route_v4_best_gw query: dst 0.0.0.0
2022-05-21 03:41:37 net_route_v4_best_gw result: via 192.168.1.1 dev eth0
2022-05-21 03:41:37 ROUTE_GATEWAY 192.168.1.1/255.255.255.0
2022-05-21 03:41:37 TUN/TAP device tun0 opened
2022-05-21 03:41:37 net_iface_mtu_set: mtu 1500 for tun0
2022-05-21 03:41:37 net_iface_up: set tun0 up
2022-05-21 03:41:37 net_addr_v4_add: 10.41.0.15/21 dev tun0
2022-05-21 03:41:37 net_route_v4_add: 127.0.0.1/32 via 192.168.18.1 dev [NULL] table 0 metric -1
2022-05-21 03:41:37 net_route_v4_add: 0.0.0.0/1 via 10.41.0.1 dev [NULL] table 0 metric -1
2022-05-21 03:41:37 net_route_v4_add: 128.0.0.0/1 via 10.41.0.1 dev [NULL] table 0 metric -1
2022-05-21 03:41:37 net_route_v4_add: 2.2.2.2/32 via 192.168.1.1 dev [NULL] table 0 metric -1
2022-05-21 03:41:37 Initialization Sequence Completed
```

You should verify that your exit IP has changed:


```
curl https://wtfismyip.com/json
{
    "YourFuckingIPAddress": "51.159.55.86",
    "YourFuckingLocation": "Paris, IDF, France",
    "YourFuckingHostname": "51-159-55-86.rev.poneytelecom.eu",
    "YourFuckingISP": "Scaleway",
    "YourFuckingTorExit": false,
    "YourFuckingCountryCode": "FR"
}
```
