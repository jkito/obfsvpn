#!/bin/bash

function datef() {
    # Output:
    # 2019 Aug 1 20:29:08
    date "+%Y %b %-d %T"
}

# Get the containers "service name"
# https://github.com/docker/compose/issues/1503#issuecomment-755244228
getServiceName() {
  dig -x "$(hostname -i)" +short | cut -d _ -f 2
}

function createConfig() {
    CLIENT_PATH=$1
    cd "$APP_PERSIST_DIR" || exit 1

    # Redirect stderr to the black hole
    cat << EOF | /usr/share/easy-rsa/easyrsa build-client-full "$CLIENT_ID" nopass
    yes
EOF
    # Writing new private key to '/usr/share/easy-rsa/pki/private/client.key
    # Client sertificate /usr/share/easy-rsa/pki/issued/client.crt
    # CA is by the path /usr/share/easy-rsa/pki/ca.crt

    mkdir -p "$CLIENT_PATH"

    cp "$APP_PERSIST_DIR/pki/private/$CLIENT_ID.key" "$APP_PERSIST_DIR/pki/issued/$CLIENT_ID.crt" "$APP_PERSIST_DIR/pki/ca.crt" /etc/openvpn/ta.key "$CLIENT_PATH"

    # Set default value to HOST_ADDR if it was not set from environment
    if [ -z "$HOST_ADDR" ]
    then
        HOST_ADDR='localhost'
    fi

    cd "$APP_INSTALL_PATH" || exit 1
    cp config/client.ovpn "$CLIENT_PATH"
    # dirty hack: copy client config to root of APP_PERSIST_DIR
    # for reusing in the obfsvpn-client container
    cp config/client.ovpn "$APP_PERSIST_DIR/"


    # Embed client authentication files into config file
    cat <(echo -e '\n<ca>') \
        "$CLIENT_PATH/ca.crt" <(echo -e '</ca>\n<cert>') \
        "$CLIENT_PATH/$CLIENT_ID.crt" <(echo -e '</cert>\n<key>') \
        "$CLIENT_PATH/$CLIENT_ID.key" <(echo -e '</key>\n<tls-auth>') \
        "$CLIENT_PATH/ta.key" <(echo -e '</tls-auth>') \
        >> "$CLIENT_PATH/client.ovpn"
}
