#!/bin/bash

set -e

kill_foreground() {
    echo "Control plane exited. Killing server."
    ps -C "obfsvpn-server" -o "%p"| grep -v PID | xargs kill;
}

kill_background() {
    echo "Server exited. Killing background process."
    kill 0;
}

cd "${APP_INSTALL_PATH}"

# check that all environment variables are set
requiredVars=(
  CONTROL_PORT
  OBFSVPN_STATE
  OBFSVPN_LOCATION
  OBFS4_IP
  OBFS4_PORT
  OBFS4_HOST
  OPENVPN_HOST
  OPENVPN_PORT
  OBFS4_DATA_DIR
  OBFS4_KEY_FILE
)

for name in "${requiredVars[@]}"; do
    if [[ -z "${!name}" ]]; then
        echo "Required environment variable $name not set!"
        QUIT=true
    fi
done
if [[ ${QUIT} ]]; then
    echo -e "\nStarting obfsvpn failed.\n"
    exit 1;
fi

# ensure the control plane is stopped once we leave the script
trap "exit" INT TERM
trap "kill_background" EXIT

# start the control plane in background
./control &
export PID1=$!

# track existance of the background process, kill server if it's gone
( tail -f --pid=$PID1 /dev/null; kill_foreground ) &


if [[ "$HOP_PT" == "1" ]]; then
    if [[ "$KCP" == "1" ]]; then
      # start the obfsvpn server in udp hopping mode + kcp
      ./obfsvpn-server --hop \
          --kcp \
          --addr "${OBFS4_HOST}" \
          --remote "${OPENVPN_HOST}:${OPENVPN_PORT}" \
          --state "$OBFS4_DATA_DIR" \
          --config "${OBFS4_KEY_FILE:-$OBFS4_DATA_DIR/obfs4.json}" \
          --persist="${PERSIST_BRIDGE_STATE:-false}" \
          -v
      fi
    # start the obfsvpn server in udp hopping mode
    ./obfsvpn-server --hop \
        --addr "${OBFS4_HOST}" \
        --remote "${OPENVPN_HOST}:${OPENVPN_PORT}" \
        --state "$OBFS4_DATA_DIR" \
        --config "${OBFS4_KEY_FILE:-$OBFS4_DATA_DIR/obfs4.json}" \
        --persist="${PERSIST_BRIDGE_STATE:-false}" \
        -v
elif [[ "$KCP" == "1" ]]; then
    # start the obfsvpn server in obfs4 kcp mode
    ./obfsvpn-server \
        --kcp \
        --udp \
        --addr "${OBFS4_HOST}" \
        --port "${OBFS4_PORT}" \
        --remote "${OPENVPN_HOST}:${OPENVPN_PORT}" \
        --state "$OBFS4_DATA_DIR" \
        --config "${OBFS4_KEY_FILE:-$OBFS4_DATA_DIR/obfs4.json}" \
        --persist="${PERSIST_BRIDGE_STATE:-false}" \
        -v
else
     # start the obfsvpn server in regular obfs4 mode
    ./obfsvpn-server \
        --udp \
        --addr "${OBFS4_HOST}" \
        --port "${OBFS4_PORT}" \
        --remote "${OPENVPN_HOST}:${OPENVPN_PORT}" \
        --state "$OBFS4_DATA_DIR" \
        --config "${OBFS4_KEY_FILE:-$OBFS4_DATA_DIR/obfs4.json}" \
        --persist="${PERSIST_BRIDGE_STATE:-false}" \
        -v
fi
