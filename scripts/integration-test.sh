#!/bin/bash

mode=$1

print_logs() {
    echo "Integration test stopped. Getting logs."
    docker compose -p "$mode" logs
}

trap "exit" INT TERM
trap "print_logs" EXIT

if [[ -n $mode ]]; then echo "Running mode: $1"; fi

env_file=./.env
if [[ "$mode" == "hop" ]]; then
  env_file=./.env.hopping
elif [[ "$mode" == "kcp" ]]; then
  env_file=./.env.kcp
elif [[ "$mode" == "hop-kcp" ]]; then
  env_file=./.env.hopping.kcp
fi

docker compose -p "$mode" down --rmi all -v
docker compose -p "$mode" build --no-cache --parallel
docker compose -p "$mode" --env-file $env_file up -d --build

# need to wait for openvpn to generate configs
max_retry=40
counter=0

# Testing bridged tunnel
until docker compose -p "$mode" --env-file $env_file exec client ping -c 3 -I tun0 8.8.8.8
do
   ((counter++))
   [[ counter -eq $max_retry ]] && echo "Failed!" && exit 1
   echo "Pinging in client container with config $(cat ${env_file} | grep KCP) and $(cat ${env_file} | grep HOP_PT) failed. Trying again. Try #$counter"
   sleep 30
done


# Testing bridge control panel
if [[ `docker compose -p "$mode" logs | grep control-client | tail -n1 | cut -d "|" -f2 | xargs` == "parsing failure" ]]
then
  echo "failed to parse from control panel"
  exit 1
else
  echo "control panel parsing succeeded"
fi;
